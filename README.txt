= Workflow Submit =

Convert workflow radio options into submit-style buttons.


== Hooks Implemented ==

: hook_form_alter()
  Primary logic for this module to convert radio options to submit buttons


== Additional Behaviors ==

=== Install ===
 - Change the weight of this module to be after the workflow module

